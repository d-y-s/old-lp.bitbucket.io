/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
//=require ../_blocks/**/_*.js


/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

    'use strict';

    /**
     * определение существования элемента на странице
     */
    $.exists = (selector) => $(selector).length > 0;

    /**
     * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
     */
    //=require ../_blocks/**/[^_]*.js

    $("#video-play").YTPlayer({
        mute: false,
        showControls: false,
        useOnMobile: false,
        abundance: 0.1,
        quality: 'highres'
    });

    $('.main-nav__nav a').on('click', function(e){
        var anchor = $(this);
        
        $('.mob-panel__menu').removeClass('open-menu');
        $('.menu-btn').removeClass('menu-btn_active');

        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 800);
        e.preventDefault();
        return false;
    });

    var getHeightPlacementContacts = $('.placement__contacts').outerHeight();
    $('.placement__map').css('height', getHeightPlacementContacts);

    ymaps.ready(function () {  
        var map = new ymaps.Map("map", {
            center: [55.76, 37.64], 
            zoom: 10
        });
    
        if (map) {
            ymaps.modules.require(['Placemark', 'Circle'], function (Placemark, Circle) {
                var placemark = new Placemark([55.37, 35.45]);
            });
        }
    });

    

});
