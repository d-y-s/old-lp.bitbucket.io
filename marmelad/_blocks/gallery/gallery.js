$('.gallery__items').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    dots: false,
    smartSpeed: 800,
    margin: 10,
    navText: ['', ''],
    responsive: {
        320: {
            items: 2
        },
        480: {
            items: 3
        },
        768: {
            items: 3
        }
    }
});

lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
})