
var fixedPanel = $('.fixed-panel');
        
$(window).scroll(function(){
    if ($(this).scrollTop() > 100 && fixedPanel.hasClass('fixed-panel')) {
        fixedPanel.addClass('fixed');
    }
    else if ($(this).scrollTop() <= 100 && fixedPanel.hasClass('fixed')) {
        fixedPanel.removeClass('fixed');
    }
});