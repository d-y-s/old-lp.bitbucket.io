$('.menu-btn').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('menu-btn_active');

    $('.mob-panel__menu').toggleClass('open-menu');
});

$('.mob-panel__phones').on('click', function(e) {
    $('.mob-panel__phone').toggleClass('open-phone');
});

$(document).on('click', function(e) {
    if($(e.target).closest('.mob-panel__section').length){
        return;
    }
    $('.mob-panel__menu').removeClass('open-menu');
    $('.menu-btn').removeClass('menu-btn_active');
    $('.mob-panel__phone').removeClass('open-phone');
});