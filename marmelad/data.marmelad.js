module.exports = {
  app: {
    lang: 'ru',
    stylus: {
      theme_color: '#3E50B4',
    },
    GA: false, // Google Analytics site's ID
    package: 'ключ перезаписывается значениями из package.json marmelad-сборщика',
    settings: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    storage: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    buildTime: '',
    controls: [
      'default',
      'brand',
      'success',
      'info',
      'warning',
      'danger',
    ],
  },

  advantagesItem: [
    {
      'icon': 'advantages-icon.png',
      'name': 'Развитая инфраструктура и подъездные пути',
      'desc': 'Не требует дополнительных подъездных дорог, Участок располагается вплотную к Новорижскому шоссе',
      'incNumber':'inc-num-1'
    },
    {
      'icon': 'advantages-icon2.png',
      'name': 'Инженерные коммуникации',
      'desc': 'На территории участка',
      'incNumber':'inc-num-2'
    },
    {
      'icon': 'advantages-icon3.png',
      'name': 'Возможна покупка в ИПОТЕКУ',
      'desc': 'От продавца — условия выгоднее, чем в банке!',
      'incNumber':'inc-num-3'
    },
    {
      'icon': 'advantages-icon4.png',
      'name': 'Государственная поддержка',
      'desc': 'Подана заявка на вступление в Государственую программу «Российские Технопарки»',
      'incNumber':'inc-num-4'
    },
    {
      'icon': 'advantages-icon5.png',
      'name': 'Юридическая безопасность',
      'desc': 'Посмотреть оригинал выписки из ЕГРН',
      'incNumber':'inc-num-5'
    },
    {
      'icon': 'advantages-icon6.png',
      'name': 'Возможна продажа частями от 1 Га',
      'desc': 'Общая площадь 30 ГЕКТАРОВ',
      'incNumber':'inc-num-6'
    }
  ],

  galleryItem: [
    {
      'img': 'gallery-img.jpg'
    },
    {
      'img': 'gallery-img2.jpg'
    },
    {
      'img': 'gallery-img3.jpg'
    },
    {
      'img': 'gallery-img2.jpg'
    }
  ],
  pageTitle: 'marmelad',
};
